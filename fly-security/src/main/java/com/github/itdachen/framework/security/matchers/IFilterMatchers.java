package com.github.itdachen.framework.security.matchers;

/**
 * Description:
 * Created by 王大宸 on 2022-09-23 17:23
 * Created with IntelliJ IDEA.
 */
public interface IFilterMatchers {

    String[] matchers();

}
