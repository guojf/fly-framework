package com.github.itdachen.framework.security.constants;

/**
 * Description: 浏览器安全配置常量
 * Created by 王大宸 on 2022-09-23 10:04
 * Created with IntelliJ IDEA.
 */
public class SecurityBrowserConstants {

    /**
     * session失效默认的跳转地址
     */
    public static final String DEFAULT_SESSION_INVALID_URL = "/session/invalid";

    /**
     * 默认登录页面
     */
    public static final String DEFAULT_LOGIN_PAGE_URL = "/signIn";

    /**
     * 重定向属性
     */
    public static final String REDIRECT_URI = "redirect_uri";

}
