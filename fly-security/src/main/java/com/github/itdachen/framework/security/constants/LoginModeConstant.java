package com.github.itdachen.framework.security.constants;

/**
 * Description:
 * Created by 王大宸 on 2022-09-23 16:40
 * Created with IntelliJ IDEA.
 */
public class LoginModeConstant {

    /**
     * 手机用户不存在,在进行根据用户号查询用户,用户不存在
     */
    public static final String NO_MOBILE_USER = "NO_MOBILE_USER";

    /**
     * 根据用户名密码查询时,用户不存在
     */
    public static final String NOT_HAVE_USER = "NOT_HAVE_USER";

}
